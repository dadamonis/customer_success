import pandas as pd
import warnings
import time


warnings.filterwarnings("ignore")

path = '~/Downloads/customer/gap/fo_assessment/data/final_assessment_data/products/'
file = 'SKU_GAP_201809280000002.csv'

data_file = path + file

data_df = pd.read_csv(data_file,
                        sep="|",
                        header=0,
                        index_col=False,
                        #parse_dates=['Date'],
                        dayfirst=True,
                        tupleize_cols=False,
                        error_bad_lines=True,
                        warn_bad_lines=True,
                        skip_blank_lines=True
                        )

data_types = pd.DataFrame(data_df.dtypes, columns=['Data Type'])
missing_data_counts = pd.DataFrame(data_df.isnull().sum(), columns=['Missing Values'])
present_data_counts = pd.DataFrame(data_df.count(), columns=['Present Values'])
unique_value_counts = pd.DataFrame(columns=['Unique Values'])
for v in list(data_df.columns.values):
    unique_value_counts.loc[v] = [data_df[v].nunique()]


data_quality_report = data_types.join(present_data_counts).join(missing_data_counts).join(unique_value_counts)
header_list = list(data_quality_report)
header_list.append("data_quality_notes")
header_list.insert(0, "maps_to_celect_schema")
data_quality_report = data_quality_report.reindex(columns = header_list)

"""Schema Mapping"""

products_dict = {'sku_nbr'      : 'sku',
                 'color'        : 'color',
                 'size_um_cde'  : 'size',
                 'rtl_price'    : 'msrp'}

orders_dict = {'ORDER_NO'           : 'orderId',
               'ORDER_DATE'         : 'createDateTime',
               'SHIPMENT_LINE_NO'   : 'shipmentId',
               'ZIP_CODE'           : 'zipCode',
               'ITEM_ID'            : 'sku',
               'UNIT_WEIGHT'        : 'weight',
               'ORDERED_QTY'        : 'quantityOrdered',
               'SHIPNODE_KEY'       : 'storeId',
               'SHIPMENT_QUANTITY'  : 'quantityFulfilled',
               '10'                 : 'quantityDeclined'}

stores_dict = {'STR_ID'             : 'storeId',
               'ZIPCODE'            : 'zipCode',
               'USEFORFULFILLMENT'  : 'enableShipFromStore',
               '4'                  : 'shippingMethods'}

inventory_dict = {'INV_DATE': 'dateId',
                  'SKU_ID'  : 'sku',
                  '143'     : 'storeNumber',
                  'OH_QTY'  : 'onHandInventory'}

transactions_dict = {'sls_corr_seq_nbr' : 'transactionDate',
                     'sku_id'           : 'sku',
                     'node_key'         : 'fulfilledLocationId',
                     '4'                : 'creditedLocationId',
                     'trn_qty'          : 'units',
                     '6'                : 'revenue',
                     '7'                : 'priceStatus',
                     'trn_nbr'          : 'uniqueTransactionId'}

shipping_dict = {'1': 'storeType',
                 '2': 'shipFromCountry',
                 '3': 'shipToCountry',
                 '4': 'shipFromState',
                 '5': 'shipToState',
                 '6': 'postalZone',
                 '7': 'serviceLevelCode',
                 '8': 'serviceLevel',
                 '9': 'carrier',
                 '10': 'weightCeiling',
                 '11': 'cost'}

data_quality_report['maps_to_celect_schema'] = data_quality_report.index.map(products_dict)
data_quality_report.sort_values(by=['maps_to_celect_schema'], inplace = True)

data_quality_report.to_csv("~/Downloads/dq_rpt_" + file +".csv")
print("\nData Quality Report: ", file, "\n")
#print("Total records: {}".format(len(data_df.index)))
print(data_quality_report)

