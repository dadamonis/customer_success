import pandas as pd


# CELECT DATA FILE PATH VARIABLES
celect_or_file = input('Enter Celect Override File: ')
celect_inv_file = input('Enter Celect Inventory Snapshot File: \n')
# ALDO DATA FILE PATH VARIABLES
aldo_dirty_inv_file = input('Enter ALDO  Dirty Inventory File: ')
aldo_inv_file = input('Enter ALDO Inventory Snapshot File')

# CREATE CELECT DATAFRAMES
celect_or_df = pd.read_csv(celect_or_file)
celect_inv_df = pd.read_csv(celect_inv_file)

# CREATE ALDO DATAFRAMES
aldo_dirty_df = pd.read_csv(aldo_dirty_inv_file)  # DROP LAST ROW FROM THE DF??
aldo_inv_df = pd.read_csv(aldo_inv_file)

# ZERO OUT DIRTY INVENTORY
celect_id_sku = celect_inv_df.store_id + celect_inv_df.sku.astype(int)
aldo_id_sku = aldo_dirty_df.NODE_KEY + aldo_dirty_df.ITEM_ID
celect_inv_df.loc[celect_id_sku.isin(aldo_id_sku), 'quantity'] = 0

# CELECT DF MERGE
celect_merge_df = pd.merge(
  celect_or_df, celect_inv_df,
  left_on=['celectFulfilledStore', 'sku'],
  right_on=['store_id', 'sku'])

celect_merge_df = celect_merge_df[[
  'brandName', 'orderId', 'orderCreateDate',
  'orderCreateDateTime', 'sku', 'inventoryVal', 'celectFulfilledStore',
  'celect_fulfill_store_type', 'OMSFulfilledStore', 'oms_fulfill_store_type',
  'quantity', 'time_stamp']]

celect_merge_df.rename(columns={
  'celect_fulfill_store_type': 'celect_store_type',
  'oms_fulfill_store_type': 'oms_store_type',
  'quantity': 'celect_inv_snapshot',
  'time_stamp': 'celect_inv_timestamp',
  'inventoryVal': 'celect_inventory_value'
}, inplace=True)

celect_merge_df.to_csv(
  '~/Downloads/aldo/prod/inventory_rpts/celect_merge.csv'
  )

# ALDO + CELECT MERGE

aldo_mege_df = pd.merge(
  celect_merge_df, aldo_inv_df,
  left_on=['celectFulfilledStore', 'sku'],
  right_on=['SHIPNODE_KEY', 'ITEM_ID'],
  how='outer')

aldo_mege_df.to_csv(
  '~/Downloads/aldo/prod/inventory_rpts/11_07/11_07_US_aldo_outer_merge.csv')


# print(aldo_mege_df.head(40))
