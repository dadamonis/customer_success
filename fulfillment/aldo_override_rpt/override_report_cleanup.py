import pandas as pd
import numpy as np
import argparse


def read_data():
	rawData = file
	if region == 'us':
		df = pd.read_csv((rawData), skiprows=[0])
	else:
		df = pd.read_csv(rawData)

	# Create new type columns to track store type
	df['celect_fulfill_store_type'] = 'store'
	df['oms_fulfill_store_type'] = 'store'

	# Compare celectFulfilledStore to OMSFulfilledStore - if values don't match Celect has been overridden.
	or_df = df[df.celectFulfilledStore != df.OMSFulfilledStore].sort_values(['orderCreateDate', 'celectFulfilledStore'])
	or_df.dropna(inplace=True)
	# print(or_df.head(10))

	return or_df


def states(or_df):
	# Assign store_type
	#print(x.head(2))
	or_df.loc[or_df['celectFulfilledStore'].isin(dc_us), 'celect_fulfill_store_type'] = 'distribution center (us)'
	or_df.loc[or_df['celectFulfilledStore'].isin(mh_us), 'celect_fulfill_store_type'] = 'mini-hub (us)'
	 
	new_or_df = or_df[['brandName', 'orderId', 'orderCreateDate', 'orderCreateDateTime', 'sku', 'inventoryVal',
	                   'celectFulfilledStore', 'celect_fulfill_store_type', 'OMSFulfilledStore',
	                   'oms_fulfill_store_type']]

	# new_or_df.drop_duplicates(subset=['orderId', 'sku'], keep='last')
	# print(new_or_df.head(5))

	new_or_df.to_csv('~/Downloads/aldo/prod/override_reports/us_truncated_override_LATEST.csv',  sep=',')


def canada(or_df):
	# Assign store_type
	or_df.loc[or_df['celectFulfilledStore'].isin(dc_ca), 'celect_fulfill_store_type'] = 'distribution center (ca)'
	or_df.loc[or_df['celectFulfilledStore'].isin(mh_ca), 'celect_fulfill_store_type'] = 'mini-hub (ca)'
	 
	new_or_df = or_df[['brandName', 'orderId', 'orderCreateDate', 'orderCreateDateTime', 'sku', 'inventoryVal',
	                   'celectFulfilledStore', 'celect_fulfill_store_type', 'OMSFulfilledStore',
	                   'oms_fulfill_store_type']]

	new_or_df.drop_duplicates(subset=['orderId', 'sku'], keep='last')
	new_or_df.to_csv('~/Downloads/aldo/prod/override_reports/ca_truncated_override_LATEST.csv', sep=',')


# Lists of Aldo's DCs and Mini-Hubs
dc_us = [9920, 9923]
mh_us = [2283]

dc_ca = [9910, 9911]
mh_ca = [1488]

parser = argparse.ArgumentParser()
parser.add_argument('--region',  choices=['ca', 'us'], help="Must be either 'ca' for Canada or 'us' for United States")
parser.add_argument("--file",  help="Used to pass the file to process - include path if necessary")
args = parser.parse_args()
region = args.region
file = args.file

if file.endswith('.csv'):
	print("Processing file...")
else:
	print("Incorrect file format. Exiting.")
	exit()

if region == 'us':
	or_df=read_data()
	brand = or_df.iloc[0] [ 'brandName']
	if brand != 'aldo_us':
		print('ERROR: Data in file is for brand:', brand, 'and --region was set to:', region)
		exit()
	else:
		states(or_df)

if region == 'ca':
	or_df=read_data()
	brand = or_df.iloc[0] [ 'brandName']
	if brand != 'aldo_ca':
		print('ERROR: Data in file is for brand:', brand, 'and --region was set to:', region)
		exit()
	else:
		canada(or_df)
